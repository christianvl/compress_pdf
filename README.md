# Compress PDF

<p>This is a simple Bash script to compress PDF files using ghostscript

## Instructions 

<p>The default setting is "ebook" for compression

<p>One valid file must be provided. You can pass a file when calling the command and defining an output file name:

```bash
compress_pdf input_file output_file 
```

<p>If no output is defined, the script will default to "compressed_file"

<p>When no file is provided, the script will try to call the system GUI dialog to select a file.

## License
<p> This software is licensed under the [AGPL3](https://www.gnu.org/licenses/agpl-3.0.html) license.
<p> ![AGPL](https://www.gnu.org/graphics/agplv3-88x31.png "AGPL3")
