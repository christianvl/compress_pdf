#!/usr/bin/env bash
#Christian Fairlie Pearson van Langendonck
#This is a script for compressing a PDF file

function getInput {
    if [ -n "$1" ]
    then
        input=$1
    else
        if [ -x "$(command -v kdialog)" ]
        then
            input=$(kdialog --getopenfilename . "PDF files (*.pdf)")
            if [ ! $? -eq 0 ]
            then
                echo No file selected.
                exit 1
            fi
        elif [ -x "$(command -v zenity)" ] && [ -z "$input" ]
        then
            input=$(zenity --file-selection --filename "${HOME}/")
            if [ ! $? -eq 0 ]
            then
                echo No file selected.
                exit 1
            fi
        else
            echo No compatible GUI available and no file was provided.
            echo CLI usage: $0 input_file.pdf output_file.pdf
            exit 1
        fi
    fi
}

function checkFile {
    if [ ! -f $input ]
    then
        echo No valid file selected.
        exit 1
    fi
}

function compress {
    echo "compressing $input to $output"
    gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile="$2" "$1"
    if [ $? -eq 0 ]
    then
        echo Created new compressed file $2
        exit 0
    else
        echo No action taken
        exit 1
    fi
}

getInput $1
if [ $? -eq 0 ]
then
    checkFile
    if [ $? -eq 0 ]
    then
        if [ -z "$2" ]
        then
            if [ -x "$(command -v kdialog)" ]
            then
                output=$(kdialog --getsavefilename "$input" "PDF files (*.pdf)")
            else
                output=compressed_file.pdf
            fi
        else
            output=$2
        fi
        compress "$input" "$output"
    fi
fi
